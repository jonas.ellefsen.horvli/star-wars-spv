# Star-Wars-spv
Hjemmeoppgave gitt av Sparebanken Vest, gjennomført av Jonas Horvli

# Fremgangsmåte:
## HjemmeOppgave 1: Star Wars React App
  
### Planets

Etter å ha lest oppgaven var det første naturlig å starte et nytt react prosjekt med create-react-app, og så lage et git repo.
Fjernet så all den autogenererte koden og oppdaterte gitignore for å ignorere IntelliJ sine filer i prosjektet.
  
Det første problemet jeg bestemte meg for å løse var å kartlegge state håndteringen av komponenten, og etablere kontakt med star-wars APIet. 
Her støttet jeg i et lite problem hvor jeg ikke fikk koblet meg opp med APIet og fikk melding at requesten var blokkert av CORS policy. Brukte rundt 40 min før jeg skjønte at problemet var det at jeg manglet en “/” på slutten av request urlen.
  
Etterpå var det bare å printe ut informasjonen på siden som jeg var interessert i, før jeg la til styling i form av css.
  
### PlanetView
  
Her var prosessen egentlig veldig lik, det eneste som ikke var repeterende fra forrige siden Planets, var at komponenten skulle kunne hente informasjon fra urlen.
Hvis en bruker Router i stedet for BrowserRouter får man lett tilgang til url parametere gjennom this.props.match.params.<nameOfParam>.
  
### Konklusjon
  
På grunn av begrenset tid valgte jeg den enkleste fremgangsmåten i bygging av applikasjonen. Selv bruker jeg ofte redux til å håndtere state, men konfigurering kan fort ta lang tid. Som sagt kom jeg bare borti 1 problem når jeg først skulle koble appen opp til apiet, ellers var utviklingen ganske smidig og referer gjerne til git historikken hvis du vil gå mer i detalj i fremgangsmåten min!


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.
