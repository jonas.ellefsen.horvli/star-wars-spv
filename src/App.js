import React from 'react';
import { Router, Route } from 'react-router-dom';
import './App.css';
import Planets from "./components/Planets";
import { createBrowserHistory } from 'history';
import PlanetView from "./components/PlanetView";

export const history = createBrowserHistory();

function App() {
  return (
    <div className="App">
        <Router history={history}>
            <Route exact path={'/'} component={Planets}/>
            <Route exact path={'/planet/:id'} component={PlanetView}/>
        </Router>
    </div>
  );
}

export default App;
