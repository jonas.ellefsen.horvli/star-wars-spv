import React from 'react';
import {getPlanet} from "../utils/planet-api-functions";
import * as renderIf from 'render-if';
import './PlanetView.css';
import {indentNumber} from "../utils/helper-functions";

class PlanetView extends React.Component {
    state = {
        planet: null,
        isLoading: false
    };

    componentDidMount() {
        this.setState({ isLoading: true });
        this.loadPlanet();
    }

    async loadPlanet() {
        const planet = await getPlanet(this.props.match.params.id);
        this.setState({ planet, isLoading: false });
    }

    render() {
        const id = this.props.match.params.id;
        const {planet, isLoading} = this.state;
        return (
            <div>
                {renderIf(isLoading)(() => (
                    <div className='loader'>Loading planet...</div>
                ))}
                {renderIf(!isLoading && planet)(() => (
                    <div className='planetView'>
                        <h1>{planet.name}</h1>
                        <div className='imageContainer'>
                            <img className='planetViewImg'
                                 src={`https://starwars-visualguide.com/assets/img/planets/${id}.jpg`}
                                 alt={'planet.img'}/>
                        </div>
                        <div className='planetViewContent'>
                            <div>{`Population: ${indentNumber(planet.population)}`}</div>
                            <div>{`Climate: ${planet.climate}`}</div>
                            <div>{`Terrain: ${planet.terrain}`}</div>
                            <div>{`Diameter: ${indentNumber(planet.diameter)}`}</div>
                        </div>
                    </div>
                ))}
            </div>
        );
    }
}

export default PlanetView;