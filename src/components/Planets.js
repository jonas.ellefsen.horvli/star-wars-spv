import React from 'react';
import {getPlanets} from "../utils/planet-api-functions";
import * as renderIf from 'render-if';
import './Planets.css';
import {indentNumber} from "../utils/helper-functions";

class Planets extends React.Component {
    state = {
        planets: [],
        next: 'https://swapi.co/api/planets/',
        isLoading: false
    };

    componentDidMount() {
        this.loadPlanets();
    }

    async loadPlanets() {
        this.setState({ isLoading: true });
        const data = await getPlanets(this.state.next);
        const planets = this.state.planets.concat(data.results);
        const next = data.next;
        this.setState({ planets, next, isLoading: false });
    }

    render() {
        return (
            <div>
                <h1>Planets</h1>
                {renderIf(this.state.isLoading)(() => (
                    <div className='loader'>Loading planets...</div>
                ))}
                {renderIf(this.state.planets.length > 0)(() => (
                    <div>
                        {this.state.planets.map((planet, i) => {
                            return (
                            <a key={i} className='planet' href={`/planet/${i + 2}`}>
                                <img className='planetImg'
                                    src={`https://starwars-visualguide.com/assets/img/planets/${i + 2}.jpg`}
                                    alt={'planet.img'}/>
                                <div className='planetContent'>
                                    <h3>{planet.name}</h3>
                                    <div>{`Population: ${indentNumber(planet.population)}`}</div>
                                </div>
                            </a>
                        )})}
                        {renderIf(!this.state.isLoading)(() => (
                        <button
                            className='loadMoreButton'
                            onClick={() => this.loadPlanets()}
                            disabled={this.state.isLoading}>
                            Load More
                        </button>
                        ))}
                    </div>
                ))}
            </div>
        );
    }
}

export default Planets;