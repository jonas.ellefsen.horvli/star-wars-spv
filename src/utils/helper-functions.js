
export function indentNumber(number) {
    return parseInt(number) ? parseInt(number).toLocaleString('en') : number;
}