import axios from 'axios';

export async function getPlanets(url = 'https://swapi.co/api/planets/') {
    try {
        const response = await axios.get(url);
        return response.data;
    } catch (e) {
        console.error(`Failed to get planets: ${e.message}`);
    }
}

export async function getPlanet(id) {
    try {
        const response = await axios.get(`https://swapi.co/api/planets/${id}`);
        return response.data;
    } catch (e) {
        console.error(`Failed to get planet: ${e.message}`);
    }
}